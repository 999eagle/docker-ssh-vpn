FROM alpine:3.9

RUN apk add --no-cache openssh && \
    adduser -D user && \
    mkdir /home/user/.ssh && \
    chown -R user:user /home/user/.ssh && \
    chmod -R 0700 /home/user/.ssh

ADD entrypoint.sh /entrypoint.sh

EXPOSE 22
CMD ["/bin/sh","/entrypoint.sh"]
