#!/bin/sh

for key_type in rsa dsa ecdsa ed25519; do
	if [[ ! -f /ssh/host_${key_type}_key || ! -f /ssh/host_${key_type}_key.pub ]]; then
		rm -f /ssh/host_${key_type}_key /ssh/host_${key_type}_key.pub
		/usr/bin/ssh-keygen -t ${key_type} -f /ssh/host_${key_type}_key -N ""
	fi
	cp /ssh/host_${key_type}_key /etc/ssh/ssh_host_${key_type}_key
	cp /ssh/host_${key_type}_key.pub /etc/ssh/ssh_host_${key_type}_key.pub
	chmod 600 /etc/ssh/ssh_host_${key_type}_key
	chmod 644 /etc/ssh/ssh_host_${key_type}_key.pub
done

if [[ -z $PASSWORD ]]; then
	PASSWORD=$(date +%s | sha256sum | base64 | head -c 32 ; echo )
	echo "Generated password for user `user`: $PASSWORD"
fi

echo "user:$PASSWORD" | chpasswd

# To disable authentication with password, uncomment the `PasswordAuthentication no` line
cat <<EOF >>/etc/ssh/sshd_config
Match User user
	PermitTunnel yes
	AllowTcpForwarding yes
	X11Forwarding no
	# PasswordAuthentcation no
EOF

exec /usr/sbin/sshd -D -e
