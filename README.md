# SSH Tunnel Server

This container acts as a ssh server for tunneling network connections through a SOCKSv5 proxy.

## Getting started

### Server setup

```bash
docker pull registry.gitlab.com/999eagle/docker-ssh-vpn
touch authorized_keys
docker run -d -p "8022:22" --restart always --cap-add "NET_ADMIN" -v "$(pwd)/ssh_data:/ssh" -v "$(pwd)/authorized_keys:/home/user/.ssh/authorized_keys" registry.gitlab.com/999eagle/docker-ssh-vpn
```

Example `docker-compose.yml` file:

```yaml
version: '3'
services:
  ssh_tunnel:
    image: registry.gitlab.com/999eagle/docker-ssh-vpn
    container_name: ssh_tunnel
    cap_add:
      - NET_ADMIN
    ports:
      - "8022:22"
    restart: always
    volumes:
      - ./ssh_data:/ssh
      - ./authorized_keys:/home/user/.ssh/authorized_keys
```

### Connecting from a client

```bash
ssh -D 1234 -p 8022 -N user@YOUR_SERVER_URL
```

Then use `localhost:1234` as a SOCKSv5 proxy and you're done! You can check whether it works by checking your public ip, for example using [whatismyip](https://www.whatismyip.com).

## More info

### Host keys

SSH host keys will be generated if they don't exist in `/ssh`. Binding this path to a volume allows keeping the same host keys between restarts of the container.

### Authentication

Public keys of SSH clients can be added to the `authorized_keys` file to allow authentication without a password. If you want to authenticate using a password, either set the `PASSWORD` environment variable to your password or use the generated password printed at startup.
